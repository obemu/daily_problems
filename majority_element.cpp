//  Hi, here's your problem today. This problem was recently asked by AirBNB:

//  A majority element is an element that appears more than
//  half the time. Given a list with a majority element,
//  find the majority element.

//  Here's an example and some starting code.

#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <vector>

int majority_element(std::vector<int>& nums);

int main() {
  std::vector<int> nums = {3, 5, 3, 3, 2, 4, 3};  // majority is 3
  std::printf("majority_element: %d\n", majority_element(nums));
  return 0;
}

int majority_element(std::vector<int>& nums) {
  if (nums.empty()) return -1;

  std::map<int, int> numMap;

  for (auto&& i : nums)
    numMap[i]++;

  int majority_key = (*numMap.begin()).first;
  for (auto&& [key, val] : numMap)
    if (val > numMap[majority_key]) majority_key = key;

  return majority_key;
}