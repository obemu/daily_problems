// Hi, here's your problem today. This problem was recently
// asked by Facebook:
//  A Perfect Number is a positive integer that is equal to
//  the sum of all its positive divisors except itself.
//
//  For instance,
//  28 = 1 + 2 + 4 + 7 + 14
//
//  Write a function to determine if a number is a perfect number.

#include <iostream>

bool checkPerfectNumber(int num);

int main() {
  std::cout << "checkPerfectNumber(28): " << (checkPerfectNumber(28) ? "True" : "False")
            << '\n';
  //  True
  //  28 = 1 + 2 + 4 + 7 + 14

  return 0;
}

bool checkPerfectNumber(int num) {
  //  Has to be positive
  if (0 > num) return false;

  unsigned int divisorsSum = 0;

  for (unsigned int i = 1; i < num; i++)
    if (0 == num % i) divisorsSum += i;

  return num == divisorsSum;
}