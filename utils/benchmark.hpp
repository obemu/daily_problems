// Library used for benchmarking function execution times
// Copyright (C) 2021 Emanuel Oberholzer
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

#ifndef BENCHMARK_HPP
#define BENCHMARK_HPP

#include <chrono>
#include <tuple>
#include <type_traits>

using Duration = std::chrono::duration<double>;

template <typename Func, typename... Args>
using Result = typename std::invoke_result_t<Func, Args...>;

template <typename Func, typename... Args>
typename std::enable_if<std::is_same<Result<Func, Args...>, void>::value,
                        std::tuple<Duration>>::type
benchmark(Func function, Args&&... args) {
  auto start = std::chrono::steady_clock::now();
  function(std::forward<Args>(args)...);
  auto end = std::chrono::steady_clock::now();
  return std::make_tuple(end - start);
}

template <typename Func, typename... Args>
typename std::enable_if<!std::is_same<Result<Func, Args...>, void>::value,
                        std::tuple<Duration, Result<Func, Args...>>>::type
benchmark(Func function, Args&&... args) {
  Result<Func, Args...> result;
  auto start = std::chrono::steady_clock::now();
  result = function(std::forward<Args>(args)...);
  auto end = std::chrono::steady_clock::now();
  return std::make_tuple(end - start, result);
}

#endif  // BENCHMARK_HPP