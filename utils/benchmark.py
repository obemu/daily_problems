from typing import Any, Callable, Tuple
import time as time


def benchmark(function: Callable, *args) -> Tuple[int, Any]:
    """
    Returns a tuple containing the time it took to call and
    execute function in ns and the result of function.
    """

    start = time.perf_counter_ns()
    result = function(*args)
    return (time.perf_counter_ns()-start, result)
