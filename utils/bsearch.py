from typing import Any, Callable, List, Protocol

Comparator = Callable[[Any, Any], int]


def bsearch(key, left: int, right: int, container: List, comparator: Comparator) -> int:
    """
    Returns the index of key if found, otherwise -1.

    The first argument provided to the comparator is key, the second
    is the element at the current middle.
    """

    while left <= right:
        middle = left + ((right - left)//2)
        result = comparator(key, container[middle])

        if 0 == result:
            return middle

        if 0 > result:
            # result is negative, key is in upper half
            left = middle+1
        else:
            # result is positve, key is in lower half
            right = middle-1

    return -1


def bsearch_special(key, left: int, right: int, container, comparator: Comparator, retriever: Callable[[Any, int], Any]) -> int:
    """
    Returns the index of key if found, otherwise -1.

    The first argument provided to the comparator is key, the second
    is the element at the current middle.

    Same as bsearch, but the Callable retriever is used to get
    an element at a specified index from container.
    """

    while left <= right:
        middle = left + ((right - left)//2)
        result = comparator(key, retriever(container, middle))

        if 0 == result:
            return middle

        if 0 > result:
            # result is negative, key is in upper half
            left = middle+1
        else:
            # result is positve, key is in lower half
            right = middle-1

    return -1
