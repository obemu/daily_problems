# Works!

# Example:
#
# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
# Output: 7 -> 0 -> 8
# Explanation: 342 + 465 = 807.
#
# Here is the function signature as a starting point (in Python):

# Definition for singly-linked list.

import time as time
from typing import ClassVar
start = time.time()


class ListNode(object):
    def __init__(self, x, next=None):
        self.val = x
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode):
        # return self.recursive(l1, l2)
        return self.iterative(l1, l2)

    def iterative(self, l1: ListNode, l2: ListNode):
        if None == l1:
            return l2

        if None == l2:
            return l1

        len_l1 = 0
        len_l2 = 0
        longest = None
        shortest = None

        longest = l1
        while longest.next:
            longest = longest.next
            len_l1 += 1

        longest = l2
        while longest.next:
            longest = longest.next
            len_l2 += 1

        if len_l1 >= len_l2:
            longest = l1
            shortest = l2
        else:
            longest = l2
            shortest = l1

        head = longest
        carry = 0
        val = 0

        while shortest.next:
            val = longest.val + shortest.val + carry
            carry = int(val/10)

            if 0 != carry:
                val -= 10

            longest.val = val

            shortest = shortest.next
            longest = longest.next

        # handle last element
        val = longest.val + shortest.val + carry
        carry = int(val/10)

        if 0 != carry:
            val -= 10
        longest.val = val

        if 0 != carry:
            longest = longest.next
            val = longest.val + carry
            carry = int(val/10)
            longest.val = val

            if 0 != carry:
                longest.val -= 10
                longest.next = ListNode(carry)

        return head

    # def recursive(self, l1: ListNode, l2: ListNode, carry=0):
    #     if l1 == None:
    #         return l2

    #     if l2 == None:
    #         return l1

    #     val = l1.val + l2.val + carry

    #     if val < 10:
    #         l1.val = val
    #         l1.next = self.recursive(l1.next, l2.next)
    #     else:
    #         l1.val = 0
    #         l1.next = self.recursive(l1.next, l2.next, int(val/10))
    #         pass

    #     return l1


# l1 = ListNode(0)
# l1.next = ListNode(4)
# l1.next.next = ListNode(5)
# l1.next.next.next = ListNode(9)
l1 = ListNode(2)
l1.next = ListNode(4)
l1 .next.next = ListNode(3)

# l2 = ListNode(0)
# l2.next = ListNode(6)
# l2.next.next = ListNode(7)
# l2.next.next.next = ListNode(2)
# l2.next.next.next.next = ListNode(1)
l2 = ListNode(5)
l2.next = ListNode(6)
l2 .next.next = ListNode(4)

result = Solution().addTwoNumbers(l1, l2)
print()
while result:
    print(result.val, end=" ")
    result = result.next

print("\n")

print(f"duration: {(time.time()-start)*1000000}us\n")
