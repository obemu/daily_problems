# Hi, here's your problem today. This problem was recently asked by Twitter:
#
# Given an array, nums, of n integers, find all unique
# triplets (three numbers, a, b, & c) in nums such
# that a + b + c = 0. Note that there may not be any
# triplets that sum to zero in nums, and that the
# triplets must not be duplicates.
#
# Example:
#
# Input: nums = [0, -1, 2, -3, 1]
# Output: [0, -1, 1], [2, -3, 1]
#
# Here's a starting point:


class Solution(object):

    def threeSum(self, nums: list):
        triplets = []

        for i in nums:
            for j in nums:
                for k in nums:
                    if 0 == i and 0 == j and 0 == k:
                        continue

                    if 0 == i + j + k and not self.contains(triplets, [i, j, k]):
                        triplets.append([i, j, k])

        return triplets

    def contains(self, lists: list, keyList: list):
        for item in lists:
            if self.containsElements(item, keyList):
                return True

        return False

    def containsElements(self, src: list, key: list):
        """
        Check if src contains all elements of key
        """

        for i in key:
            if i not in src:
                return False

        return True


# Test Program
nums = [1, -2, 1, 0, 5]
print(Solution().threeSum(nums))
# [[-2, 1, 1]]
