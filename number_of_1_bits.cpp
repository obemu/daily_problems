// Hi, here's your problem today. This problem was recently asked by Google:
// Given an integer, find the number of 1 bits it has.
// Here's an example and some starting code.

#include <iostream>

unsigned int one_bits(int num);

int main() {
  const int number = 23;

  std::printf("one bits in %d: %u\n", number, one_bits(number));
  // 4
  // 23 = 0b10111

  return 0;
}

unsigned int one_bits(int num) {
  unsigned int amount = 0;

  for (; num; num >>= 1)
    amount += (num & 0x01);

  return amount;
}