// Hi, here's your problem today. This problem was
// recently asked by AirBNB:
// Pascal's Triangle is a triangle where all numbers are
// the sum of the two numbers above it. Here's an example
// of the Pascal's Triangle of size 5.
//
// 0.        1
// 1.       1 1
// 2.      1 2 1
// 3.     1 3 3 1
// 4.    1 4 6 4 1
//
// Given an integer n, generate the n-th row of the Pascal's Triangle.

#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>
#include <vector>

#include "utils/benchmark.hpp"

template <typename T>
void printVector(const std::vector<T>& vector, std::stringstream& stream);

std::vector<int> pascalTriangleRow(int n);

int main() {
  for (size_t i = 1; i < 8; ++i) {
    std::stringstream stream;

    auto [elapsed1, vec] = benchmark(pascalTriangleRow, i);
    auto [elapsed2] = benchmark(printVector<int>, vec, stream);

    std::cout << "Elapsed pascalTriangleRow(" << i << "): " << elapsed1.count()
              << '\n';
    std::cout << "Elapsed printVector: " << elapsed2.count() << '\n';
    std::cout << "Result: \n" << stream.str() << '\n';
  }

  return 0;
}

template <typename T>
void printVector(const std::vector<T>& vector, std::stringstream& stream) {
  stream << '[';
  size_t i;
  for (i = 0; i < vector.size() - 1; ++i)
    stream << vector[i] << ", ";
  stream << vector[i] << "]\n";
}

std::vector<int> pascalTriangleRow(int n) {
  assert(1 <= n);

  --n;
  std::vector<int> row = {1};
  std::vector<int> rowCopy;

  rowCopy.reserve(n);

  for (size_t i = 1; i < n + 1; ++i) {
    std::copy(row.begin(), row.end(), rowCopy.begin());
    for (size_t k = 1; k < i; ++k)
      row[k] = rowCopy[k - 1] + rowCopy[k];
    row.push_back(1);
  }

  return row;
}
