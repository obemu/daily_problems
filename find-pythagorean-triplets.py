# Given a list of numbers, find if there
# exists a pythagorean triplet in that list.
# A pythagorean triplet is 3 variables
# a, b, c where a² + b² = c²
#
# Example:
#
# Input: [3, 5, 12, 5, 13]
# Output: True
#
# Here, 5² + 12² = 13².

def findPythagoreanTriplets(nums: list) -> bool:
    for i in range(len(nums)):
        c = pow(nums[i], 2)
        for j in range(len(nums)):
            a = pow(nums[j], 2)
            for k in range(len(nums)):
                if j != k and a + pow(nums[k], 2) == c:
                    return True
    return False


def findPythagoreanTripletsRecursive(nums: list, cIdx=0, aIdx=0, bIdx=0) -> bool:
    if len(nums) == bIdx:
        bIdx = 0
        aIdx = aIdx + 1

    if len(nums) == aIdx:
        aIdx = 0
        cIdx = cIdx + 1

    if len(nums) <= cIdx:
        return False

    if pow(nums[cIdx], 2) == pow(nums[aIdx], 2)+pow(nums[bIdx], 2):
        return True

    return findPythagoreanTripletsRecursive(nums, cIdx, aIdx, bIdx+1)


print("Iterative:")
print(findPythagoreanTriplets([3, 12, 5, 13]))
# True
print(findPythagoreanTriplets([3, 5, 12, 5, 13]))
# True

print("\nRecursive:")
print(findPythagoreanTripletsRecursive([3, 12, 5, 13]))
# True
print(findPythagoreanTripletsRecursive([3, 5, 12, 5, 13]))
# True
