# This problem was recently asked by Facebook:
#
# Given a matrix that is organized such that the numbers
# will always be sorted left to right, and the first number
# of each row will always be greater than the last element
# of the last row (mat[i][0] > mat[i - 1][-1]), search for
# a specific value in the matrix and return whether it exists.

from sre_constants import BRANCH
from typing import List
from utils.bsearch import bsearch_special


def elementAt(matrix: List[List], index: int):
    """
    Get the element at index of matrix, assuming
    that we contatenate each ending of a row with
    the start of the next row.
    """

    listIndex = 0
    listLength = len(matrix[listIndex])

    # Copy value of index+1, because we change the
    # value of index in the for loops, therefore
    # index cannot be used for the condition.
    loops = index+1

    for i in range(loops):
        if i >= listLength:
            listIndex += 1
            index -= listLength

            if listIndex >= len(matrix):
                return None

            listLength += len(matrix[listIndex])

    return matrix[listIndex][index]


def searchMatrix(matrix: List[List[int]], key: int) -> bool:
    """
    Use the BinarySearch algorithm to search key in
    the matrix.
    Return true if matrix contains key, else false.
    """

    totalLength = 0
    for i in range(len(matrix)):
        totalLength += len(matrix[i])

    return 0 <= bsearch_special(key, 0, totalLength-1, matrix, lambda k, v: v-k, elementAt)


mat = [
    [1, 3, 5, 8],
    [10, 11, 15, 16],
    [24, 27, 30, 31],
]

# for i in range(len(mat) * len(mat[0])):
#     element = elementAt(mat, i)
#     print("%d: %s" % (element, "True" if searchMatrix(mat, element) else "False"))


print(searchMatrix(mat, 4))
# False

print(searchMatrix(mat, 10))
# True
