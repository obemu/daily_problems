// Hi, here's your problem today. This problem was recently
// asked by Amazon:
// Given an integer, reverse the digits. Do not convert the
// integer into a string and reverse it.
// Here's some examples and some starter code.

#include <cmath>
#include <iostream>

int reverse_integer(int num);

int main() {
  int nums[] = {
      135,   // reversed: 531
      -321,  // reversed: -123
      0      // reversed: 0
  };

  for (auto&& i : nums)
    std::printf("Number %d\treversed: %d\n", i, reverse_integer(i));

  return 0;
}

int reverse_integer(int num) {
  const bool isSigned = 0 > num ? true : false;
  num = isSigned ? -num : num;

  int length = 0;
  for (int i = num; 0 != i; i /= 10, ++length)
    ;

  int digit;
  int inverted = 0;
  for (int i = 1; 0 != num; ++i, num /= 10) {
    digit = num % 10;
    inverted += digit * ((int) pow(10.f, length - i));
  }

  return isSigned ? -inverted : inverted;
}
