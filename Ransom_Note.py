# Hi, here's your problem today. This problem was recently asked by Google:
# A criminal is constructing a ransom note. In order to disguise
# his handwriting, he is cutting out letters from a magazine.
# Given a magazine of letters and the note he wants to write,
# determine whether he can construct the word.

from typing import List


class Solution(object):
    def canSpell(self, magazine: List[str], note: str) -> bool:
        for c in note:
            if c not in magazine:
                return False

        return True


print(Solution().canSpell(['a', 'b', 'c', 'd', 'e', 'f'], 'bed'))
# True

print(Solution().canSpell(['a', 'b', 'c', 'd', 'e', 'f'], 'cat'))
# False
