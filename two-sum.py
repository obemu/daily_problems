# You are given a list of numbers, and a target number k.
# Return whether or not there are two numbers
# in the list that add up to k.
#
# Example:
# Given [4, 7, 1 , -3, 2] and k = 5,
# return true since 4 + 1 = 5.
#
# Try to do it in a single pass of the list.


def two_sum(l: list, k: int):
    for i in range(0, len(l)):
        for j in range(0, len(l)):
            if i == j:
                continue
            else:
                if l[i] + l[j] == k:
                    return True

    return False


def two_sum_rec(l: list, k: int, idx1=int(0), idx2=int(0)):
    if len(l) == idx2:
        idx2 = 0
        idx1 = idx1 + 1

    if len(l) <= idx1:
        return False

    if idx1 != idx2 and l[idx1] + l[idx2] == k:
        return True

    return two_sum_rec(l, k, idx1, idx2+1)


print(f"iterative: {two_sum([4, 7, 1, -3, 2], 5)}")
print(f"iterative: {two_sum([4, 7, 4, -3, 2], 5)}")
print()
print(f"recursive: {two_sum_rec([4, 7, 1, -3, 2], 5)}")
print(f"recursive: {two_sum_rec([4, 7, 4, -3, 2], 5)}")
