# https://www.techseries.dev/daily#accordion-item-1561526783975_0

# Definition for a linked-list node.
class Node(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeTwoLists(self, a, b):
        return self.recursive(a, b)
        # return self.iterative(a, b)

    def recursive(self, a: Node, b: Node):
        if a == None:
            return b

        if b == None:
            return a

        if a.val < b.val:
            a.next = self.recursive(a.next, b)
            return a
        else:
            b.next = self.recursive(a, b.next)
            return b

    def iterative(self, a: Node, b: Node):
        head = a

        while a.next:
            if b != None and b.val < a.next.val:
                a.next = Node(b.val, a.next)
                b = b.next
                pass
            a = a.next

        # Append the rest of b to a
        a.next = Node(b.val, b.next)

        return head


# Test program
# 1 -> 3 ->5
a = Node(1)
a.next = Node(3)
a.next.next = Node(5)

# 2 -> 4 -> 6 -> 8
b = Node(2)
b.next = Node(4)
b.next.next = Node(6)
b.next.next.next = Node(8)

c = Solution().mergeTwoLists(a, b)
print()
while c is not None:
    print(c.val, end=" ")
    c = c.next
# 1 2 3 4 5 6
print("\n")
