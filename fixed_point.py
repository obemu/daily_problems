# This problem was recently asked by Apple:
#
# A fixed point in a list is where the value is equal
# to its index. So for example the list [-5, 1, 3, 4],
# 1 is a fixed point in the list since the index and
# value is the same. Find a fixed point (there can be many,
# just return 1) in a sorted list of distinct elements, or
# return None if it doesn't exist.

from utils.benchmark import benchmark
from typing import List


def find_fixed_point(nums: List[int]) -> bool:
    # List is sorted
    numsLen = len(nums)

    for i in range(numsLen):
        left = i
        right = numsLen-1

        while left <= right:
            middle = left + ((right - left)//2)
            result = middle - nums[middle]

            if 0 == result:
                return True

            if 0 > result:
                # result is negative, key is in upper half
                left = middle+1
            else:
                # result is positve, key is in lower half
                right = middle-1

    return False


def find_fixed_point_sequentiell(nums: List[int]) -> bool:
    for i in range(len(nums)):
        if i == nums[i]:
            return True
    return False


test_cases = [
    [-5, 1, 3, 4],  # True
    [-5, 0, 3, 4],  # False
    [i+1 for i in range(10000)],  # False
]


for case in test_cases:
    res_bsearch = benchmark(find_fixed_point, case)
    res_seq_search = benchmark(find_fixed_point_sequentiell, case)

    print("Case length: %u" % len(case))
    print("Find bsearch\t%uns\t%s" %
          (res_bsearch[0], "True" if res_bsearch[1] else "False"))
    print("Find seq search\t%uns\t%s\n" %
          (res_seq_search[0], "True" if res_seq_search[1] else "False"))
