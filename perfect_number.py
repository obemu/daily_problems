# Hi, here's your problem today. This problem was recently
# asked by Facebook:
# A Perfect Number is a positive integer that is equal to
# the sum of all its positive divisors except itself.
#
# For instance,
# 28 = 1 + 2 + 4 + 7 + 14
#
# Write a function to determine if a number is a perfect number.


class Solution():
    def checkPerfectNumber(self, num: int) -> bool:
        # Has to be positive
        if 0 > num:
            return False

        divisors_sum = 0

        for i in range(1, num):
            if 0 == num % i:
                divisors_sum += i

        return divisors_sum == num


print(Solution().checkPerfectNumber(28))
# True
# 28 = 1 + 2 + 4 + 7 + 14
