# This problem was recently asked by Microsoft:
# Given a string, find the length of the longest
# substring without repeating characters.

import time as t

start = t.time()


class Solution:
    def length_of_longest_substring(self, string: str):
        substring = string[0]
        longest = 0

        for i in range(1, len(string)):
            if substring.find(string[i]) == -1:
                substring += string[i]
            else:
                if longest < len(substring):
                    longest = len(substring)
                substring = ""
            pass

        return longest


print(Solution().length_of_longest_substring('abrkaabcdefghijjxxx'))


print(f"duration: {(t.time()-start)*1000000}us\n")
