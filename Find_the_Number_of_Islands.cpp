// Hi, here's your problem today. This problem was recently
// asked by LinkedIn:
//
// Given a 2-dimensional grid consisting of 1's (land blocks)
// and 0's (water blocks), count the number of islands present
// in the grid. The definition of an island is as follows:
// 1.) Must be surrounded by water blocks.
// 2.) Consists of land blocks (1's) connected to adjacent land
// blocks (either vertically or horizontally).
// Assume all edges outside of the grid are water.
//
// Example:
//
// Input:
// 10001
// 11000
// 10110
// 00000
//
// Output: 3
//
// Here's your starting point:

#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>

#define GRID_WIDTH  5
#define GRID_HEIGHT 4

bool inRange(std::vector<std::vector<int>>& grid, int x, int y);
int amountIslands(std::vector<std::vector<int>>& grid);

int main() {
  std::vector<std::vector<int>> grid = {
      {1, 1, 0, 0, 0},
      {0, 1, 0, 0, 1},
      {1, 0, 0, 1, 1},
      {0, 0, 0, 0, 0},
  };

  assert(GRID_HEIGHT == grid.size());
  for (auto&& vec : grid)
    assert(GRID_WIDTH == vec.size());

  std::printf("amount of islands: %d\n", amountIslands(grid));
  // 3

  return 0;
}

bool inRange(std::vector<std::vector<int>>& grid, int x, int y) {
  if (0 > y) return false;
  if (0 > x) return false;
  if (grid.size() <= y) return false;
  if (grid[0].size() <= x) return false;

  return true;
}

// TODO: Finish amountIslands
int amountIslands(std::vector<std::vector<int>>& grid) {
  return 0;
}