# Hi, here's your problem today. This problem was recently asked by Google:
# Given an integer, find the number of 1 bits it has.
# Here's an example and some starting code.


def one_bits(num: int) -> int:
    amount: int = 0

    while 0 != num:
        amount += num & 0x01
        num >>= 1

    return amount


print("one bits in %d: %u\n" % (23, one_bits(23)))
# 4
# 23 = 0b10111
