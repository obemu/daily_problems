# Hi, here's your problem today. This problem was recently
# asked by Amazon:
# Given an integer, reverse the digits. Do not convert the
# integer into a string and reverse it.
# Here's some examples and some starter code.

def reverse_integer(num: int) -> int:
    isSigned = True if 0 > num else False
    num = -num if 0 > num else num
    reversed: int = 0

    length: int = 0
    i = num
    while 0 != int(i):
        length += 1
        i /= 10

    i = 1
    while 0 != int(num):
        digit = int(num % 10)
        reversed += digit * (10 ** (length-i))

        i += 1
        num /= 10

    return -reversed if isSigned else reversed


nums = {135,   # reversed: 531
        -321,  # reversed: -123
        0      # reversed: 0
        }

for i in nums:
    print("Number %d\treversed: %d" % (i, reverse_integer(i)))
