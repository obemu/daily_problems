# Hi, here's your problem today. This problem was recently asked by AirBNB:
#
# A majority element is an element that appears more than
# half the time. Given a list with a majority element,
# find the majority element.
#
# Here's an example and some starting code.

from sys import maxsize
from typing import Dict, List


def majority_element(nums: List[int]) -> int:
    elements: Dict[int, int] = dict()
    for num in nums:
        if None == elements.get(num):
            elements[num] = 0
        else:
            elements[num] += 1

    keys = list(elements.keys())
    max_key = keys[0]

    for i in range(1, len(keys)):
        key = keys[i]
        if elements[key] > elements[max_key]:
            max_key = key

    return elements[max_key]


print(majority_element([3, 5, 3, 3, 2, 4, 3]))
# 3
