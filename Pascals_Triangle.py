# Hi, here's your problem today. This problem was
# recently asked by AirBNB:
# Pascal's Triangle is a triangle where all numbers are
# the sum of the two numbers above it. Here's an example
# of the Pascal's Triangle of size 5.
#
# 0.        1
# 1.       1 1
# 2.      1 2 1
# 3.     1 3 3 1
# 4.    1 4 6 4 1
#
# Given an integer n, generate the n-th row of the Pascal's Triangle.

from typing import List
from math import factorial as fac


def pascal_triangle_row(n: int) -> List[int]:
    assert 1 <= n, "n has to be greater than or equal to 1"

    n = n-1
    row = [1]

    for i in range(1, n+1):
        row_copy = row[:]

        # First number in row is always 1
        # row[0] = 1

        for k in range(1, i):
            row[k] = row_copy[k-1]+row_copy[k]

        # Last number in row is always 1
        row.append(1)

    return row


def pascal_triangle_row_ulrich(n):
    """
    Ulrich hat den algorithmus von wikipedia abgesaugt
    """
    result = []
    for j in range(n):
        result.append(fac(n - 1) // (fac(j) * fac(n - j - 1)))

    return result


# print(pascal_triangle_row(0))
# error

print(pascal_triangle_row(1))
# [1]

print(pascal_triangle_row(2))
# [1, 1]

print(pascal_triangle_row(3))
# [1, 2, 1]

print(pascal_triangle_row(4))
# [1, 3, 3, 1]

print(pascal_triangle_row(5))
# [1, 4, 6, 4, 1]

print(pascal_triangle_row(6))
# print(pascal_triangle_row_ulrich(6))
# [1, 5, 10, 10, 5, 1]

print(pascal_triangle_row(7))
# [1, 6, 15, 20, 15, 6, 1]
