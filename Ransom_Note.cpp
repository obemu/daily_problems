// Hi, here's your problem today. This problem was recently asked by Google:
// A criminal is constructing a ransom note. In order to disguise
// his handwriting, he is cutting out letters from a magazine.
// Given a magazine of letters and the note he wants to write,
// determine whether he can construct the word.

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

#include "utils/benchmark.hpp"

bool canSpell(std::vector<char> magazine, std::string note);

int main() {
  static const std::vector<char> characters = {'a', 'b', 'c', 'd', 'e', 'f'};

  auto [elapsed, res] = benchmark(canSpell, characters, "bed");
  std::cout << "Elapsed: " << elapsed.count()
            << "\ncanSpell: " << (res ? "True" : "False") << "\n\n";
  // True

  std::tie(elapsed, res) = benchmark(canSpell, characters, "cat");
  std::cout << "Elapsed: " << elapsed.count()
            << "\ncanSpell: " << (res ? "True" : "False") << "\n\n";
  // False

  return 0;
}

bool canSpell(std::vector<char> magazine, std::string note) {
  const char* c_str = note.c_str();

  for (; '\0' != *c_str; ++c_str)
    if (magazine.end() == std::find(magazine.begin(), magazine.end(), *c_str))
      return false;

  return true;
}