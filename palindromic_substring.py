# A palindrome is a sequence of characters that reads
# the same backwards and forwards. Given a string, s,
# find the longest palindromic substring in s.
#
# Example:
#
# Input: "banana"
# Output: "anana"
#
# Input: "million"
# Output: "illi"


import time as t
start = t.time()


class Solution:
    def longest_palindrome(self, s: str):
        start_idx = 0
        end_idx = -1

        while start_idx < len(s) and end_idx == -1:
            start_idx += 1
            end_idx = s.find(s[start_idx-1], start_idx)

        return s[start_idx-1:end_idx+1]


# Test program


print(Solution().longest_palindrome("tracecars"))
# racecar

print(f"duration: {(t.time()-start)*1000000}us\n")
