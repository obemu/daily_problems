# This problem was recently asked by Apple:
#
# You are given two strings, A and B. Return whether A
# can be shifted some number of times to get B.
#
# Eg. A = abcde, B = cdeab should return true because A
# can be shifted 3 times to the right to get B.
# A = abc and B= acb should return false.

from utils.benchmark import benchmark


def is_shifted(a: str, b: str) -> bool:
    bLen = len(b)

    for c in reversed(b[:]):
        if a == b:
            return True

        b = c+b[:bLen-1]

    return False


print(is_shifted('abcde', 'cdeab'))
# print(str(benchmark(is_shifted, 'abcde', 'cdeab'))+"ns")
# True
